package com.forocore.hibernate.foro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.forocore.hibernate.foro.dao.ProfesorDao;
import com.forocore.hibernate.foro.dao.PreguntaDao;
import com.forocore.hibernate.foro.model.Profesor;

@Controller
public class MainController {
	
	
	@Autowired
	private PreguntaDao preguntaDao;

	@GetMapping(value="/")
	public String welcome(Model modelo) {
		modelo.addAttribute("preguntas", preguntaDao.getAll());
		
		return "index";
		
	}
	
	/*public String listaPost(Model modelo) {
		
		return "redirect:/index";
	}
	/*@RequestMapping(value="/create")
	@ResponseBody
	public String create(String nombre, String email, String ciudad, String password) {
		Profesor user=new Profesor();
		
		user.setNombre(nombre);
		user.setEmail(email);
		user.setCiudad(ciudad);
		user.setPassword(password);
		
		//userDao.create(user);
		
		return null;
	}
	*/
	
	
	
	
	//	@RequestMapping(value="/list")
	//@ResponseBody
/*	public String list() {
		
	}*/
	
	
}
