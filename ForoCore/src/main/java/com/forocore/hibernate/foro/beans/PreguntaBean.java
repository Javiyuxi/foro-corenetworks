package com.forocore.hibernate.foro.beans;

import java.util.Date;

import com.forocore.hibernate.foro.model.Profesor;



public class PreguntaBean {
	private String url;
	private String titulo;
	private String contenido;
	private Date fecha;
	private Profesor autor;
	
	
	public Profesor getAutor() {
		return autor;
	}


	public void setAutor(Profesor autor) {
		this.autor = autor;
	}


	public String getUrl() {
		return url;
	}
	
	
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
