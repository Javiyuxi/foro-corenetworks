package com.forocore.hibernate.foro.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.forocore.hibernate.foro.beans.RespuestaBean;
import com.forocore.hibernate.foro.dao.RespuestaDao;
import com.forocore.hibernate.foro.dao.PreguntaDao;
import com.forocore.hibernate.foro.model.Respuesta;
import com.forocore.hibernate.foro.model.Pregunta;
import com.forocore.hibernate.foro.model.Profesor;



@Controller
public class RespuestaController {
	
	
	@Autowired
	private RespuestaDao respuestaDao;
	@Autowired
	private PreguntaDao preguntaDao;
	@Autowired
	private HttpSession httpSession;
	
	
	
	@PostMapping(value="/submit/newRespuesta")
	public String submitComment(@ModelAttribute("respuestaForm") RespuestaBean r, Model model) {
		
		
		Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
		
		Respuesta respuesta=new Respuesta();
		respuesta.setProfesor(profesor);
		
		Pregunta pregunta=preguntaDao.getById(r.getPregunta_id());
		respuesta.setPregunta(pregunta);
		respuesta.setContenido(r.getContenido());
		respuestaDao.create(respuesta);
		//post.getRespuestas().add(respuesta);
		//profesor.getRespuestas().add(respuesta);
		

		return "redirect:/pregunta/"+r.getPregunta_id();
	}
}
