package com.forocore.hibernate.foro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.forocore.hibernate.foro.beans.RegisterBean;
import com.forocore.hibernate.foro.dao.ProfesorDao;
import com.forocore.hibernate.foro.model.Profesor;



@Controller
public class RegisterController {
	@Autowired
	private ProfesorDao profesorDao;

	@GetMapping(value = "/signup")
	public String showForm(Model model) {
		RegisterBean b = new RegisterBean();
		//b.setNombre("Nombre de pruebas");
		model.addAttribute("profesorRegister", b);
		return "register";
	}

	@PostMapping(value = "/register")
	public String submit(@ModelAttribute("profesorRegister") RegisterBean r, Model model) {
		profesorDao.create(new Profesor(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));

		return "redirect:/profesores";
	}
}
