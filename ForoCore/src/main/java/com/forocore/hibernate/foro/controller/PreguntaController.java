package com.forocore.hibernate.foro.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.forocore.hibernate.foro.beans.RespuestaBean;
import com.forocore.hibernate.foro.beans.PreguntaBean;
import com.forocore.hibernate.foro.dao.PreguntaDao;
import com.forocore.hibernate.foro.model.Pregunta;
import com.forocore.hibernate.foro.model.Profesor;

@Controller
public class PreguntaController {
	
	
	@Autowired
	private PreguntaDao preguntaDao;
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/preguntas")
	public String listaPost(Model modelo) {
		//modelo.addAttribute("preguntas", preguntaDao.getAll());
		return "redirect:/";
	}
	
	@GetMapping(value="/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("pregunta", new PreguntaBean());
		return "submit";
	}
	
	
	@PostMapping(value="/submit/newpregunta")
	public String submit(@ModelAttribute("preguntaRegister") PreguntaBean r, Model model) {
		//postDao.create(new Pregunta(r.getTitulo(), r.getUrl(), r.getContenido()));
		
		//crear un post
		//obtener el autor desde la sesión.
		//Asignar el autor al post y hacer persistente el post.
		Pregunta pregunta = new Pregunta();

				pregunta.setTitulo(r.getTitulo());

				pregunta.setContenido(r.getContenido());

			
				

				

			Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");

				pregunta.setProfesor(profesor);

				preguntaDao.create(pregunta);
				//profesor.getPosts().add(post);
				
				
		return "redirect:/";
	}
	

	@GetMapping(value="/pregunta/{id}")
	public String detail(@PathVariable("id") long id, Model modelo) {
		//modelo.addAttribute("post", new Pregunta());
		
		//debemos comporbar si el post existe.
		
		Pregunta result = null;

				if ((result = preguntaDao.getById(id)) != null) {

					modelo.addAttribute("pregunta", result);

					modelo.addAttribute("respuestaForm", new RespuestaBean());

					return "preguntadetail";			

				} else {

					return "redirect:/";

		 	}
				
				
		
	}

}
