package com.forocore.hibernate.foro.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.forocore.hibernate.foro.dao.ProfesorDao;




@Controller
public class ProfesorController {
	@Autowired
	private ProfesorDao profesorDao;

	@GetMapping(value = "/profesores")
	public String listaAutores(Model modelo) {
		modelo.addAttribute("profesores", profesorDao.getAll());
		return "profesorlist";
	}
}