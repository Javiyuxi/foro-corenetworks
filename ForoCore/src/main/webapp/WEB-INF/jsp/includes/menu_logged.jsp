<li role="presentation"	${not empty param.inicio ? 'class="active"' : '' }><a href="/">Inicio</a></li>
<li role="presentation"	${not empty param.submit ? 'class="active"' : '' }><a href="/submit">Escribir una pregunta</a></li>
<li role="presentation"	${not empty param.profesores ? 'class="active"' : '' }><a
	href="/profesores">Profesores</a></li>		
<li role="presentation">
	<div class="btn-group" style="padding-top: 5px;">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
           ${param.profesor}&nbsp;<span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/logout">Salir</a></li>
          </ul>
        </div>
</li>
